from Google import Create_Service

import os
import io

from googleapiclient.http import MediaIoBaseDownload

CLIENT_SECRET_FILE = "client_secret.json"
API_NAME = 'drive'
API_VERSION = 'v3'
SCOPES = ["https://www.googleapis.com/auth/drive"]

service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)


'''
Exportable drive download for google-native file formats function
'''
def g_native_drive_download(file_id, download_path):
    results = service.files().get(fileId= file_id,fields= "id, name, mimeType").execute()
    docMimeType = results['mimeType']

    # build out with more mimetypes
    # current support for google docs
    mimeTypeMatchup = {
        "application/vnd.google-apps.document": {
            "exportType":"application/vnd.openxmlformats-officedocument.wordprocessingml.document","docExt":"docx"
            }
        }

    exportMimeType =mimeTypeMatchup[docMimeType]['exportType']
    docExt =mimeTypeMatchup[docMimeType]['docExt']
    docName = results['name']
    request = service.files().export_media(fileId=file_id, mimeType=exportMimeType)

    dir_fd = os.open(download_path, os.O_RDONLY)

    def opener(path, flags):
        return os.open(path, flags, dir_fd=dir_fd)

    fh = io.FileIO(docName+"."+docExt, mode='w', opener=opener)

    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
        print("Download %d%%." % int(status.progress() * 100))
    print(f"printing {docName} from g_native")



'''
Module level runnable code
executes on run of module
    requires completion of query variable
'''
# query = "name='<file name>'"
# response = service.files().list(q=query).execute()
# id = response['files'][0]['id']
# FILE_ID = id


# results = service.files().get(fileId= FILE_ID,fields= "id, name, mimeType").execute()
# docMimeType = results['mimeType']


# mimeTypeMatchup = {
#   "application/vnd.google-apps.document": {
#       "exportType":"application/vnd.openxmlformats-officedocument.wordprocessingml.document","docExt":"docx"
#       }
# }

# exportMimeType =mimeTypeMatchup[docMimeType]['exportType']
# docExt =mimeTypeMatchup[docMimeType]['docExt']
# docName = results['name']
# request = service.files().export_media(fileId=FILE_ID, mimeType=exportMimeType) # Export formats : https://developers.google.com/drive/api/v3/ref-export-formats

# # define the path we wish to save our new file to locally
# dir_fd = os.open("/Users/samalboher/Downloads", os.O_RDONLY)

# # python opener function pulled from docs for passing file destination to FileIO method
# def opener(path, flags):
#     return os.open(path, flags, dir_fd=dir_fd)

# fh = io.FileIO(docName+"."+docExt, mode='w', opener=opener)


# downloader = MediaIoBaseDownload(fh, request)
# done = False
# while done is False:
#     status, done = downloader.next_chunk()
#     print("Download %d%%." % int(status.progress() * 100))
