import os
import io
from googleapiclient.http import MediaIoBaseDownload
from Google import Create_Service

CLIENT_SECRET_FILE = "client_secret.json"
API_NAME = 'drive'
API_VERSION = 'v3'
SCOPES = ["https://www.googleapis.com/auth/drive"]

service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)


def drive_download(file_id, file_name, download_path):
    request = service.files().get_media(fileId = file_id)

    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fd=fh, request=request)

    done = False

    while not done:
        status, done = downloader.next_chunk()
        print('download progress {0}'.format(status.progress() * 100))

    # reset our position in binary stream back to start position
    fh.seek(0)
    with open(os.path.join(download_path, file_name), 'wb') as f:
        f.write(fh.read())
        f.close()

    print(f"printing {file_name} from drive_download")

'''
Module level runnable code
executes on run of module
    requires completion of query variable
'''
# file_ids = ['<file uris go here>']
# file_names = ['file names with extensions go here']

# for file_id, file_name in zip(file_ids, file_names):
#     request = service.files().get_media(fileId = file_id)

#     fh = io.BytesIO()
#     downloader = MediaIoBaseDownload(fd=fh, request=request)

#     done = False

#     while not done:
#         status, done = downloader.next_chunk()
#         print('download progress {0}'.format(status.progress() * 100))

#     # reset our position in binary stream back to start position
#     fh.seek(0)
#     with open(os.path.join('/Users/samalboher/Downloads', file_name), 'wb') as f:
#         f.write(fh.read())
#         f.close()
