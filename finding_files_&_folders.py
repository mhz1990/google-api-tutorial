
from Google import Create_Service
from download_non_google_file import drive_download
from download_google_file import g_native_drive_download

CLIENT_SECRET_FILE = "client_secret.json"
API_NAME = 'drive'
API_VERSION = 'v3'
SCOPE = ['https://www.googleapis.com/auth/drive']

# Create api service objectt
service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPE)

# list of parameters found at:  https://developers.google.com/drive/api/reference/rest/v3/files/list
#list of search query terms and operators:  https://developers.google.com/drive/api/guides/ref-search-terms
#  craft query string

query = "name='<file name to search for'" # input query terms of choose here

# save file object in
response = service.files().list(q=query).execute()


id = response['files'][0]['id']
name = response['files'][0]['name']


"""
Warning commented out mimeTypes are not supported in g_native_drive_download

Unless you want to add them and there conversion mimeType to mimeTypeMatchup dictionary in download_google_files.py
"""
g_mime_types = [
    # "application/vnd.google-apps.audio",
    "application/vnd.google-apps.document",
    # "application/vnd.google-apps.drive-sdk",
    # "application/vnd.google-apps.drawing",
    # "application/vnd.google-apps.file",
    # "application/vnd.google-apps.folder",
    # "application/vnd.google-apps.form",
    # "application/vnd.google-apps.fusiontable",
    # "application/vnd.google-apps.jam",
    # "application/vnd.google-apps.map",
    # "application/vnd.google-apps.photo",
    # "application/vnd.google-apps.presentation",
    # "application/vnd.google-apps.script",
    # "application/vnd.google-apps.shortcut",
    # "application/vnd.google-apps.site",
    # "application/vnd.google-apps.spreadsheet"
]


if response['files'][0]['mimeType'] in g_mime_types:
    g_native_drive_download(id, '<download path>') # ex: I used '/Users/samalboher/Downloads' as my download path on mac

else:
    drive_download(id, name, '<download path>') # ex: I used '/Users/samalboher/Downloads' as my download path on mac

print(response)
