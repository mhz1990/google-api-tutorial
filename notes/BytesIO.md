What is BytesIO?
    BytesIO is a class in Pythons IO module which provides a file-like interface for in-memory byte streams. Essentially a convenient way to manipulate binary data in-memory without the need fror actual files

Io module docs: https://docs.python.org/3/library/io.html

    With this module we are able to create a binary stream of in-memory data which we can then write to any where we desire
    ex:
        create a binary string:
            data = b"hello, BytesIO."

        transform the binary string into a byte stream
            byte_stream = BytesIO(data)

        READ Bytestream:
            content = byte_stream.read()

        Writing to Bytestream:
            byte_stream.write(b"lots more text. Like an ocean of text")
        **Note**
            When writing to an existing bytestream the write/read position(think of like a cursor) must be reset to the zeroth position.
            Ex:
                byte_stream.seek(0) #sets stream position back to zeroth position
