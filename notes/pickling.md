What is pickling?
    Pickling is the converting of a python object into a byte stream to store in a file or database, maintain program state accross sessions, or transport data over a network

    To serialize in python(convert to binary) we use:
        pickle.dump(<item_to_pickle>,<storage_file>)
    To deserialize and read (read binary as python code) we use:
        pickle.load(<file_to_deserialize>)
    For strings use pickle.dumps() and pickle.loads()
    To deserialize use unpickler

bits vs Bytes:
    bits are the smallest unit of data represented as either a 0 or 1.
    Bytes are a series of 8-bits allowing us to interpret bit in a meaningful way

What is a byte stream?
    A series of bytes used to input and output information.The difference between a bit stream and a byte stream is the decision to interpret the stream in 8 part groups instead of single discrete units(individual bits).
    A bit stream becomes a byte stream when its interpretted as a series of 8 bit sequences rather than a sequence of individual 0's and 1's
    Byte streams are important because they allow us to represent data meaningfully. For example: the same byte stream(in this case is only 1Byte) which represents 102 numerically can represent the letter "f" in ascii.

When are Byte streams used?
    Byte streams are used when the nature of data isnt certain

Unpickling:
    When a byte stream is unpickled the pickle module creates an instance of the original object first then populates the object with the correct data
        To do this the byte stream only contains the data specific to the original object instance
